/*******************************************************************************
 * Copyright (C) 2019 nicmus inc. (jivko.sabev@gmail.com)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.nicmus.jpoweradmin.api;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.nicmus.jpoweradmin.model.orm.User;
import com.nicmus.jpoweradmin.service.AccountServiceDAO;
import com.nicmus.jpoweradmin.service.RegistrationService;

@Path("/user")
public class UserService {
		
	@HeaderParam(com.nicmus.jpoweradmin.api.HeaderAPIFilter.API_HEADER_NAME)
	private String apiKey;
	
	@Inject
	private UserServiceDAO userServiceDAO;
	
	@Inject
	private AccountServiceDAO accountServiceDAO;
	
	@Inject
	private RegistrationService registrationService;
	
	/**
	 * Get the user corresponding the the API key. 
	 * @return the user for the given API key
	 */
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public User getUser() {
		return this.userServiceDAO.getUserByApi(this.apiKey);
	}

	/**
	 * Get the user corresponding the the given user name
	 * @param userName the username corresponding to user to return
	 */
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("/{UserName}")
	public User getUser(@PathParam("UserName") String userName){
		User callingUser = this.userServiceDAO.getUserByApi(this.apiKey);
		if(!callingUser.isRoot()){
			throw new WebApplicationException(Response.status(Status.UNAUTHORIZED).build());
		}
		return this.userServiceDAO.getUserByName(userName);
	}
	
	/**
	 * Update the given user. If the caller is not part of the root group, then
	 * only the user corresponding to the given API key is updated.
	 * The username cannot be changed. 
	 * @param user
	 */
	@PUT
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public void updateUser(User user) {		

		User callingUser = this.getUser();

		if(user.getUserName() == null) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("userName tag is required").build());
		}
		//root users are allowed to edit other users
		//if not root and trying to edit other users, disallow
		if(!user.getUserName().equalsIgnoreCase(callingUser.getUserName()) &&
				!callingUser.isRoot()) {
			throw new WebApplicationException(Response.status(Status.FORBIDDEN).entity("Not authorized").build());
		}		
		this.userServiceDAO.updateUser(user);
	}

	/**
	 * Create a user
	 * Only root accounts can create users
	 * @param user
	 */
	@POST
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public void addUser(User user) {
		User callingUser = this.getUser();
		if(!callingUser.isRoot()) {
			throw new WebApplicationException(Response.status(Status.FORBIDDEN).entity("Not Authorized").build());
		}
		//check if required fields are there
		if(user.getUserName() == null || user.getPassword() == null) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("username and password required!").build());
		}
		this.registrationService.register(user);
	}
	
	/**
	 * 
	 * @param userName
	 */
	@DELETE
	@Path("/{username}")
	public void deleteUser(@PathParam("username") String userName) {
		User callingUser = this.getUser();
		if(callingUser.getUserName().equals(userName) || callingUser.isRoot()) {
			this.accountServiceDAO.disableUserAcount(userName);
		} else {
			throw new WebApplicationException(Response.status(Status.FORBIDDEN).entity("Not Authorized").build());
		}
	}
}
