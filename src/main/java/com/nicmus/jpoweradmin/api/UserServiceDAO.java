/*******************************************************************************
 * Copyright (C) 2019 nicmus inc. (jivko.sabev@gmail.com)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.nicmus.jpoweradmin.api;

import java.security.MessageDigest;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.xml.bind.DatatypeConverter;

import com.nicmus.jpoweradmin.model.orm.User;
import com.nicmus.jpoweradmin.model.orm.User_;
import com.nicmus.jpoweradmin.utils.JPowerAdmin;

/**
 * Service layer for User API
 * @author jsabev
 *
 */
@Stateless
public class UserServiceDAO {

	@Inject @JPowerAdmin
	private EntityManager entityManager;
	
	@Inject
	private MessageDigest messageDigest;
		
	/**
	 * Return the user corresponding to the given API key
	 * @param apiKey the API key 
	 * @return user corresponding to API
	 */
	public User getUserByApi(String apiKey) {
		CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		CriteriaQuery<User> userQuery = criteriaBuilder.createQuery(User.class);
		Root<User> from = userQuery.from(User.class);
		//load the roles for the given user
		from.fetch(User_.roles, JoinType.INNER);
		Predicate predicate = criteriaBuilder.equal(from.get(User_.apikey), apiKey);

		//only return users that have not been flagged for deletion
		Predicate and = criteriaBuilder.and(predicate, criteriaBuilder.isFalse(from.get(User_.deleted)));
		return this.entityManager.createQuery(userQuery.select(from).where(and)).getSingleResult();
	}
	
	/**
	 * 
	 * @param newUser
	 */
	public void updateUser(User newUser) {
		User toUpdate = this.getUserByName(newUser.getUserName());
		//update password if changed
		if(newUser.getPassword() != null) {
			if(!toUpdate.getPassword().equals(newUser.getPassword())) {
				byte[] digest = this.messageDigest.digest(newUser.getPassword().getBytes());
				String passwordhash = DatatypeConverter.printBase64Binary(digest);
				toUpdate.setPassword(passwordhash);
			}
			
		}
		//update the rest of the properties
		if(newUser.getFirstName() != null) {
			toUpdate.setFirstName(newUser.getFirstName());
		}
		if(newUser.getLastName() != null) {
			toUpdate.setLastName(newUser.getLastName());
		}
		if(newUser.getEmail() != null) {
			toUpdate.setEmail(newUser.getEmail());			
		}
		if(newUser.getTimeZoneId() != null) {
			toUpdate.setTimeZoneId(newUser.getTimeZoneId());
		}
		if(newUser.getLocaleName() != null) {
			toUpdate.setLocaleName(newUser.getLocaleName());
			
		}
		this.entityManager.persist(toUpdate);		
	}

	/**
	 * Return the user corresponding to the given user name
	 * @param userName
	 * @return
	 */
	public User getUserByName(String userName) {
		CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		CriteriaQuery<User> userCriteriaQuery = criteriaBuilder.createQuery(User.class);
		Root<User> from = userCriteriaQuery.from(User.class);
		Predicate predicate = criteriaBuilder.equal(from.get(User_.userName), userName);
		return this.entityManager.createQuery(userCriteriaQuery.select(from).where(predicate)).getSingleResult();		
	}
	
	
}
